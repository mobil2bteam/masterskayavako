//
//  RPAboutViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAboutViewController.h"

@interface RPAboutViewController ()

@property (weak, nonatomic) IBOutlet UILabel *aboutProjectLabel;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end

@implementation RPAboutViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *patternImage = [UIImage imageNamed:@"pattern.png"];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:patternImage];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tabBarController.navigationItem.title = @"О проекте";
}

#pragma mark - Actions

- (IBAction)rateButtonPressed:(id)sender {
    NSString *iTunesLink = @"https://itunes.apple.com/us/app/tvorceskaa-masterskaa-vako/id1179720049?l=uk&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

@end
