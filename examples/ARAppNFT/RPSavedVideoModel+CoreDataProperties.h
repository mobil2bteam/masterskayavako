//
//  RPSavedVideoModel+CoreDataProperties.h
//  CoreAnimationTest
//
//  Created by Ruslan on 8/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RPSavedVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RPSavedVideoModel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSNumber *videoID;
@property (nullable, nonatomic, retain) NSNumber *bookID;

@end

NS_ASSUME_NONNULL_END
