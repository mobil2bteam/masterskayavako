//
//  RPCustomView.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCustomView.h"

@implementation RPCustomView

- (void)layoutSubviews{
    [super layoutSubviews];
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                     byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                           cornerRadii:CGSizeMake(80.0, 80.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end
