//
//  UIViewController+Alert.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const RPWarningMessage = @"Заполните необходимые поля";
static NSString * const RPErrorMessage = @"Произошла ошибка";
static NSString * const RPConnectionErrorMessage = @"Проверьте, пожалуйста, соединение с интернетом";

@interface UIViewController (Alert)

- (void)showMessage:(NSString *)message withTitle:(NSString *)title;

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler;

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler
   andRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler
     andBackHandler:(void (^)(UIAlertAction *action))backHandler;

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

- (void)addMBProgressHUD;

- (void)hideMBProgressHUD;

@end
