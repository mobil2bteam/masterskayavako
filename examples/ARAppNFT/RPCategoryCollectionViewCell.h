//
//  RPCategoryCollectionViewCell.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPBookCollectionViewCellDelegate.h"
@class RPBook;

@interface RPCategoryCollectionViewCell : UICollectionViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *booksCollectionView;

@property (strong, nonatomic) NSArray <RPBook *> *filteredBooksArray;

@property (weak, nonatomic) id<RPBookCollectionViewCellDelegate> delegate;

- (void)configureCellWith:(NSArray <RPBook *> *)filteredBooksArray;

@end
