//
//  RPPictureListViewController.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPBook;

@interface RPPictureListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *picturesCollectionView;

//выбранная книга, изображения которой нужно отобразить
@property (strong, nonatomic) RPBook *selectedBook;

@end
