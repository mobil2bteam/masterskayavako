//
//  RPSavedVideoModel+CoreDataProperties.m
//  CoreAnimationTest
//
//  Created by Ruslan on 8/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RPSavedVideoModel+CoreDataProperties.h"

@implementation RPSavedVideoModel (CoreDataProperties)

@dynamic path;
@dynamic videoID;
@dynamic bookID;

@end
