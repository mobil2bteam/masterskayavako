//
//  RPBookListViewController.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPBook;

@interface RPBookListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectionView;

- (void)didSelectBook:(RPBook *)book;

@end
