//
//  RPSavedVideoModel.h
//  CoreAnimationTest
//
//  Created by Ruslan on 8/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface RPSavedVideoModel : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "RPSavedVideoModel+CoreDataProperties.h"
