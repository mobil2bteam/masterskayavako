//
//  RPNavigationController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/22/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPNavigationController.h"

@interface RPNavigationController ()

@end

@implementation RPNavigationController

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
