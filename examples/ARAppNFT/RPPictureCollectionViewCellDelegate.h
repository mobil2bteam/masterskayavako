//
//  RPPictureCollectionViewCellDelegate.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPPictureCollectionViewCellDelegate <NSObject>

- (void)didSelectPictureAtIndex:(NSInteger)index;

- (void)didDownloadButtonPressed:(NSInteger)index selected:(BOOL)selected;

@end
