//
//  RPStreamVideoViewController.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPStreamVideoViewController : UIViewController

@property (strong, nonatomic) NSString *videoUrl;

@property (strong, nonatomic) NSString *path;

@end
