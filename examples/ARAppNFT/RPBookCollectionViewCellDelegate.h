//
//  RPBookCollectionViewCellDelegate.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RPBook;

@protocol RPBookCollectionViewCellDelegate <NSObject>

- (void)didSelectBook:(RPBook *)book;

@end
