//
//  RPCategoryCollectionViewCell.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCategoryCollectionViewCell.h"
#import "RPBookCollectionViewCell.h"
#import "RPOptions.h"

@implementation RPCategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.booksCollectionView registerNib:[UINib nibWithNibName:@"RPBookCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPBookCollectionViewCell class])];
    self.booksCollectionView.delegate = self;
    self.booksCollectionView.dataSource = self;
}

- (void)configureCellWith:(NSArray <RPBook *> *)filteredBooksArray{
    self.filteredBooksArray = filteredBooksArray;
    [self.booksCollectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filteredBooksArray.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPBookCollectionViewCell *cell = (RPBookCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPBookCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWith:self.filteredBooksArray[indexPath.row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    RPBook *selectedBook = self.filteredBooksArray[indexPath.row];
    [self.delegate didSelectBook:selectedBook];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = CGRectGetHeight(self.booksCollectionView.bounds);
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        width = (CGRectGetWidth(self.booksCollectionView.bounds) - 60)/4.5;
    } else {
        width = (CGRectGetWidth(self.booksCollectionView.bounds) - 30)/2.5;
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 10, 0, 10); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

@end
