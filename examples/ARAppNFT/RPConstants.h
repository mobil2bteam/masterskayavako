//
//  RPConstants.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#ifndef RPConstants_h
#define RPConstants_h

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define RPUserDefaults [NSUserDefaults standardUserDefaults]

static NSString * const RPVKIdentifier = @"5331790";
static NSString * const RPAddressSite =  @"http://www.moimastera.ru";
static NSString * const RPWasFirstLoad = @"wasFirstLoad";


#endif /* RPConstants_h */
