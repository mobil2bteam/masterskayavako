//
//  RPStreamVideoViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPStreamVideoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface RPStreamVideoViewController ()

@property (nonatomic, strong) AVPlayerViewController *controller;

@end

@implementation RPStreamVideoViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    NSURL *url;
    // если есть путь к локальному видео, то запускаем его, иначе загружаем видео по ссылке
    if (self.path) {
        url = [[NSURL alloc]initFileURLWithPath:[NSString stringWithFormat:@"%@/%@", [self documentDirectory], self.path]];
    } else {
        url = [NSURL URLWithString:self.videoUrl];
    }
    AVPlayer *player = [AVPlayer playerWithURL:url];
    self.controller = [[AVPlayerViewController alloc]init];
    self.controller.player = player;
    [self addChildViewController:self.controller];
    [self.view addSubview:self.controller.view];
    self.controller.view.frame = self.view.frame;
}

- (void)viewDidAppear:(BOOL)animated{
    [self.controller.player play];
}

- (NSString *)documentDirectory{
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
}

@end
