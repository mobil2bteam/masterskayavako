//
//  RPPictureListViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPictureListViewController.h"
#import "RPPictureCollectionViewCell.h"
#import "RPStreamVideoViewController.h"
#import "ARViewController.h"
#import "RPOptions.h"
#import "RPWebViewController.h"
#import "RPCustomView.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPSavedVideoModel.h"
#import "UIImageView+AFNetworking.h"
#import <AFNetworking.h>
#import <MBProgressHUD.h>
#import "UIViewController+Alert.h"

@interface RPPictureListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, RPPictureCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (nonatomic, strong) MBProgressHUD *HUG;

//массив отфильтрированных маркеров исходя из выбранной книги
@property (strong, nonatomic) NSArray <RPMarker *> *filteredMarkersArray;

@property (weak, nonatomic) IBOutlet UIButton *scanButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureCollectionViewHeightConstraint;

@end

@implementation RPPictureListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Рисунки";
    //self.tabBarController.navigationItem.title = @"Сканировать";
    self.scanButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.scanButton.titleLabel.numberOfLines = 2;
    self.scanButton.titleLabel.textAlignment = NSTextAlignmentCenter;
   
    UIImage *patternImage = [UIImage imageNamed:@"pattern.png"];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:patternImage];

    //регистрируем ячейку для коллекции картинок
    
    [self.picturesCollectionView registerNib:[UINib nibWithNibName:@"RPPictureCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPPictureCollectionViewCell class])];
    
    //добавляем кнопку [заказать книгу] в navigationItem
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]initWithTitle:@"Купить книгу" style:UIBarButtonItemStylePlain target:self action:@selector(buyBarButtonPressed)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    // добавляем обсервера к коллекции категорий книг для динамической высоты
    [self addObserver:self forKeyPath:@"picturesCollectionView.contentSize" options:NSKeyValueObservingOptionOld context:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.navigationController.navigationBar.hidden = NO;
    self.tabBarController.navigationItem.title = @"Рисунки";
}

- (void)dealloc{
    [self removeObserver:self forKeyPath:@"picturesCollectionView.contentSize"];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"picturesCollectionView.contentSize"]) {
        CGFloat newHeight = self.picturesCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.pictureCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Getters

- (NSArray <RPMarker *> *)filteredMarkersArray{
    if (_filteredMarkersArray) {
        return _filteredMarkersArray;
    } else {
        _filteredMarkersArray = [[NSMutableArray alloc]init];
        NSMutableArray <RPMarker *> *markers = [[NSMutableArray alloc]init];
        for (RPMarker *marker in [RPOptions sharedOptions].markers) {
            if (marker.book_id == self.selectedBook.ID) {
                [markers addObject:marker];
            }
        }
        _filteredMarkersArray = [markers copy];
        return _filteredMarkersArray;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filteredMarkersArray.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPPictureCollectionViewCell *cell = (RPPictureCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPPictureCollectionViewCell class]) forIndexPath:indexPath];
    [self configureCell:cell indexPath:indexPath];
    return cell;
}

- (void)configureCell:(RPPictureCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath{
    RPMarker *marker = self.filteredMarkersArray[indexPath.row];
    cell.pictureNameLabel.text = marker.name;
    cell.pictureImageView.image = nil;
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: marker.image]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [cell.pictureImageView setImageWithURLRequest:imageRequest
                                 placeholderImage:nil
                                          success:nil
                                          failure:nil];
    cell.playButton.tag = indexPath.row;
    cell.downloadButton.tag = indexPath.row;
    cell.delegate = self;
    //если видео уже сохранено, то устанавливаем кнопку в состояние [удалить]
    if ([self savedVideo:marker]) {
        cell.downloadButton.selected = YES;
    } else {
        cell.downloadButton.selected = NO;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = CGRectGetWidth(self.picturesCollectionView.bounds);
    width = CGRectGetWidth(self.picturesCollectionView.bounds);
    return CGSizeMake(width, width);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 0, 10, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20.0;
}

#pragma mark - RPPictureCollectionViewCellDelegate

- (void)didSelectPictureAtIndex:(NSInteger)index{
    RPSavedVideoModel *savedVideo = [self savedVideo:self.filteredMarkersArray[index]];
    RPStreamVideoViewController *streamVideoVC = [[RPStreamVideoViewController alloc]init];
    streamVideoVC.videoUrl = self.filteredMarkersArray[index].video_s;
    // если видео сохранено на телефон, то передаем его путь
    if (savedVideo) {
        streamVideoVC.path = savedVideo.path;
    }
    [self presentViewController:streamVideoVC animated:YES completion:nil];
}

- (void)didDownloadButtonPressed:(NSInteger)index selected:(BOOL)selected{
    if (selected) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Удалить видео"
                                              message:@"Вы действительно хотите удалить видео с телефона?"
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Удалить"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [self deleteVideo:self.filteredMarkersArray[index]];
                                   }];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Отменить"
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [self downloadVideo:self.filteredMarkersArray[index]];
    }
}

#pragma mark - Actions

- (IBAction)scanBooksButtonPressed:(id)sender {
    if (self.filteredMarkersArray.count) {
        ARViewController *arVC = [[ARViewController alloc]init];
        arVC.selectedBook = self.selectedBook;
        [self.navigationController pushViewController:arVC animated:YES];
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"По данному запросу нет рисунков" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - Methods

- (void)buyBarButtonPressed{
    RPWebViewController *webVC = [[RPWebViewController alloc]init];
    webVC.url = self.selectedBook.url_buy;
    [self.navigationController pushViewController:webVC animated:YES];
}

// загружает видео на телефон
- (void)downloadVideo:(RPMarker *)marker{
    self.HUG = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.HUG.mode = MBProgressHUDModeDeterminateHorizontalBar;
    self.HUG.label.text = @"Идет загрузка видео";
    __weak typeof(self) weakSelf = self;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:marker.video_m];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
//        NSLog(@"progress - %f", downloadProgress.fractionCompleted);
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.HUG.progress = downloadProgress.fractionCompleted;
        });
        
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (error) {
            [weakSelf.HUG hideAnimated:YES];
            [self showMessage:RPErrorMessage withTitle:nil];
        } else {
            [self saveVideoToDB:marker path:[response suggestedFilename]];
        }
    }];
    [downloadTask resume];
}

// сохраняет запись о видео в БД
- (void)saveVideoToDB:(RPMarker *)marker path:(NSString *)path{
    RPSavedVideoModel *savedVideo = [RPSavedVideoModel MR_createEntity];
    savedVideo.videoID = @(marker.ID);
    savedVideo.bookID = @(marker.book_id);
    savedVideo.path = path;
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            [self.picturesCollectionView reloadData];
            [self.HUG hideAnimated:YES];
        } else if (error) {
            [self.HUG hideAnimated:YES];
            [self showMessage:RPErrorMessage withTitle:nil];
        }
    }];
}

// удаляет видео с телефона
- (void)deleteVideo:(RPMarker *)marker{
    self.HUG = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.HUG.mode = MBProgressHUDModeDeterminateHorizontalBar;
    self.HUG.label.text = @"Идет удаление видео";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    RPSavedVideoModel *savedVideo = [self savedVideo:marker];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [NSString stringWithFormat:@"%@/%@",documentsPath,savedVideo.path];
    NSError *error;
    __unused BOOL success = [fileManager removeItemAtPath:path error:&error];
    [savedVideo MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        [self.HUG hideAnimated:YES];
        if (error) {
            [self showMessage:RPErrorMessage withTitle:nil];
        } else {
            [self.picturesCollectionView reloadData];
        }
    }];
}

// возвращает модель сохраненной записи о видео (если есть)
- (RPSavedVideoModel *)savedVideo:(RPMarker *)marker{
    NSInteger videoID = marker.ID;
    NSInteger bookID = marker.book_id;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"videoID == %@ && bookID == %@", @(videoID), @(bookID)];
    NSArray *videos = [RPSavedVideoModel MR_findAllWithPredicate:predicate];
    if (videos.count) {
        return videos[0];
    }
    return nil;
}

@end
