//
//  RPOptions.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPCatalogue;
@class RPBook;
@class RPMarker;
//@class RPSerie;

@interface RPOptions : NSObject <EKMappingProtocol>

+ (RPOptions *) sharedOptions;

+ (void) initWithOptions: (RPOptions *)options;

@property (nonatomic, copy) NSString *about_text;

@property (nonatomic, strong) NSArray<RPMarker *> *markers;

@property (nonatomic, strong) NSArray<RPBook *> *books;

@property (nonatomic, strong) NSArray<RPCatalogue *> *catalog;

@end


@interface RPMarker : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *video_s;

@property (nonatomic, copy) NSString *video_m;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger book_id;

@end


@interface RPBook : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *url_buy;

@property (nonatomic, copy) NSString *url_html;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger old;

@property (nonatomic, assign) NSInteger parrent_id;

@property (nonatomic, strong) NSArray<RPBook *> *series;


@end


@interface RPCatalogue : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger ID;

@end

//@interface RPSerie : NSObject <EKMappingProtocol>
//
//@property (nonatomic, copy) NSString *name;
//
//@property (nonatomic, copy) NSString *image;
//
//@property (nonatomic, copy) NSString *url_buy;
//
//@property (nonatomic, copy) NSString *url_html;
//
//@property (nonatomic, assign) NSInteger ID;
//
//@end




