//
//  RPRouter.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRouter.h"
#import "RPOptions.h"
#import "RPNavigationController.h"
#import "RPAboutViewController.h"
#import "RPBookListViewController.h"
#import "RPOnboardViewController.h"
#import "RPPictureListViewController.h"
#import "RPSerieViewController.h"
#import "RPWebViewController.h"

@implementation RPRouter

+ (RPRouter *)sharedRouter{
    static RPRouter *router = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[RPRouter alloc] init];
    });
    return router;
}

+ (void)setRootController{
    UITabBarController *tabBarVC = [[UITabBarController alloc]init];
    RPBookListViewController* allBooksVC = [[RPBookListViewController alloc]     initWithNibName:@"RPBookListViewController" bundle:nil];
    allBooksVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Все книги" image:[UIImage imageNamed:@"all_books_icon"] tag:1];
    RPAboutViewController* aboutProjectVC = [[RPAboutViewController alloc]     initWithNibName:@"RPAboutViewController" bundle:nil];
    aboutProjectVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"О проекте" image:[UIImage imageNamed:@"about_icon"] tag:2];
    tabBarVC.viewControllers = @[allBooksVC, aboutProjectVC];
    RPNavigationController *navVC = [[RPNavigationController alloc]initWithRootViewController:tabBarVC];
    [[UIApplication sharedApplication] delegate].window.rootViewController = navVC;
}

+ (void)presentOnboardingViewControllerFromController:(UIViewController *)fromController{
    RPOnboardViewController* onboardVC = [[RPOnboardViewController alloc]     initWithNibName:@"RPOnboardViewController" bundle:nil];
    [fromController presentViewController:onboardVC animated:YES completion:nil];
}

+ (void)presentSerieViewControllerFromController:(UIViewController *)fromController selectedBook:(RPBook *)selectedBook{
    RPSerieViewController* serieVC = [[RPSerieViewController alloc]     initWithNibName:@"RPSerieViewController" bundle:nil];
    serieVC.selectedBook = selectedBook;
    serieVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    serieVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [fromController presentViewController:serieVC animated:YES completion:nil];
}

+ (void)pushPictureListViewControllerFromController:(UIViewController *)fromController selectedBook:(RPBook *)selectedBook{
    /*
    UITabBarController *tabBarVC = [[UITabBarController alloc]init];
    RPPictureListViewController* pictureListVC = [[RPPictureListViewController alloc]     initWithNibName:@"RPPictureListViewController" bundle:nil];
    pictureListVC.selectedBook = selectedBook;
    pictureListVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Сканировать" image:[UIImage imageNamed:@"camera_icon"] tag:1];

    RPWebViewController *webVC = [[RPWebViewController alloc]init];
    webVC.url = selectedBook.url_html;
    webVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Помощь" image:[UIImage imageNamed:@"about_icon"] tag:2];
    tabBarVC.viewControllers = @[pictureListVC, webVC];
    [fromController.navigationController pushViewController:tabBarVC animated:YES];
     */
    RPPictureListViewController* pictureListVC = [[RPPictureListViewController alloc]     initWithNibName:@"RPPictureListViewController" bundle:nil];
    pictureListVC.selectedBook = selectedBook;
    [fromController.navigationController pushViewController:pictureListVC animated:YES];
}
@end
