//
//  RPWebViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPWebViewController.h"
#import "Masonry.h"

@implementation RPWebViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    //добавляем webView и устанавливаем констрейнты
    UIWebView *webView = [[UIWebView alloc]init];
    [self.view addSubview:webView];
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(padding);
    }];
    self.navigationItem.title = @"Заказать книгу";
    NSURL *url = [NSURL URLWithString:self.url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"Помощь";
}

@end
