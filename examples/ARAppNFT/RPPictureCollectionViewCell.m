//
//  RPPictureCollectionViewCell.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPictureCollectionViewCell.h"
#import "RPOptions.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPSavedVideoModel.h"

@implementation RPPictureCollectionViewCell

- (void)layoutSubviews{
    [super layoutSubviews];
    if (self.isGradientLayerAdded) {
        return;
    }
    UIColor *colorOne = [UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:0.1];
    UIColor *colorTwo = [UIColor colorWithRed:(123/255.0)  green:(123/255.0)  blue:(123/255.0)  alpha:1.6];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.4];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.frame = self.bounds;
    [self.pictureImageView.layer insertSublayer:headerLayer atIndex:0];
    self.isGradientLayerAdded = YES;
}

- (IBAction)downloadVideoButtonPressed:(UIButton *)sender {
    [self.delegate didDownloadButtonPressed:sender.tag selected:sender.selected];
}

- (IBAction)playButtonPressed:(UIButton *)sender {
    [self.delegate didSelectPictureAtIndex:sender.tag];
}

@end
