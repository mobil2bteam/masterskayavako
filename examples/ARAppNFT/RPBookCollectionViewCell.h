//
//  RPBookCollectionViewCell.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPBook;

@interface RPBookCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;

@property (weak, nonatomic) IBOutlet UILabel *bookNameLabel;

@property (weak, nonatomic) IBOutlet UIView *oldView;

@property (weak, nonatomic) IBOutlet UILabel *oldLabel;

- (void)configureCellWith:(RPBook *)book;

- (void)setBookNameLabelColor:(UIColor *)color;

@end
