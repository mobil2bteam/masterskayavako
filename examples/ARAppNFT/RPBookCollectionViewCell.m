//
//  RPBookCollectionViewCell.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPBookCollectionViewCell.h"
#import "RPOptions.h"
#import "UIImageView+AFNetworking.h"

@implementation RPBookCollectionViewCell

- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:self.oldView.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft)
                                           cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;
    self.oldView.layer.mask = maskLayer;
}

- (void)configureCellWith:(RPBook *)book{
    self.oldLabel.text = [NSString stringWithFormat:@"%ld+", (long)book.old];
    self.bookNameLabel.text = book.name;
    self.bookImageView.image = nil;
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: book.image]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.bookImageView setImageWithURLRequest:imageRequest
                              placeholderImage:nil
                                       success:nil
                                       failure:nil];    
}

- (void)setBookNameLabelColor:(UIColor *)color{
    self.bookNameLabel.textColor = color;
}

@end
