//
//  RPBookListViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPBookListViewController.h"
#import "RPCategoryHeaderCollectionViewCell.h"
#import "RPCategoryCollectionViewCell.h"
#import "ARViewController.h"
#import "RPOptions.h"
#import "RPBookCollectionViewCellDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import "RPSerieViewController.h"
#import "RPRouter.h"

@interface RPBookListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, RPBookCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (strong, nonatomic) NSArray <NSArray <RPBook *> *> *filteredBooksArray;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoriesCollectionViewHeightConstraint;

@end

@implementation RPBookListViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // регистрируем ячейки для коллекций
    [self.categoriesCollectionView registerNib:[UINib nibWithNibName:@"RPCategoryCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPCategoryCollectionViewCell class])];
    [self.categoriesCollectionView registerNib:[UINib nibWithNibName:@"RPCategoryHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPCategoryHeaderCollectionViewCell class])];
    
    // добавляем обсервера к коллекции категорий книг для динамической высоты
    [self addObserver:self forKeyPath:@"categoriesCollectionView.contentSize" options:NSKeyValueObservingOptionOld context:nil];
    UIImage *patternImage = [UIImage imageNamed:@"pattern.png"];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:patternImage];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tabBarController.navigationItem.title = @"Все книги";
}

- (void)dealloc{
    [self removeObserver:self forKeyPath:@"categoriesCollectionView.contentSize"];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"categoriesCollectionView.contentSize"]) {
        CGFloat newHeight = self.categoriesCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.categoriesCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

- ( NSArray <NSArray <RPBook *> *> *)filteredBooksArray{
    if (_filteredBooksArray != nil) {
        return _filteredBooksArray;
    } else {
        NSMutableArray <NSMutableArray <RPBook *> *> *catalogs = [[NSMutableArray alloc]init];
        for (RPCatalogue *catalog in [RPOptions sharedOptions].catalog) {
            NSMutableArray <RPBook *> *books = [[NSMutableArray alloc]init];
            for (RPBook *book in [RPOptions sharedOptions].books) {
                if (book.parrent_id == catalog.ID) {
                    [books addObject:book];
                }
            }
            if (books.count) {
                [catalogs addObject:books];
            }
        }
        _filteredBooksArray = [[NSArray alloc]initWithArray:catalogs];
        return _filteredBooksArray;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.filteredBooksArray.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPCategoryCollectionViewCell *cell = (RPCategoryCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPCategoryCollectionViewCell class]) forIndexPath:indexPath];
    cell.delegate = self;
    [cell configureCellWith:self.filteredBooksArray[indexPath.section]];
    return cell;
}

#pragma mark - RPBookCollectionViewCellDelegate

- (void)didSelectBook:(RPBook *)book{
    if (book.series.count) {
        [RPRouter presentSerieViewControllerFromController:self selectedBook:book];
    } else {
        [RPRouter pushPictureListViewControllerFromController:self selectedBook:book];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        width = CGRectGetWidth(self.categoriesCollectionView.bounds);
        height = ((width - 30) / 4.5) * 1.3;
        
    } else {
        width = CGRectGetWidth(self.categoriesCollectionView.bounds);
        height = ((width - 30) / 2.5) * 1.3;
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPCategoryHeaderCollectionViewCell *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPCategoryHeaderCollectionViewCell class]) forIndexPath:indexPath];
        headerView.categoryNameLabel.text = [RPOptions sharedOptions].catalog[indexPath.section].name;
        reusableview = headerView;
    }
    return reusableview;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(CGRectGetWidth(self.categoriesCollectionView.frame), 50);
}

@end
