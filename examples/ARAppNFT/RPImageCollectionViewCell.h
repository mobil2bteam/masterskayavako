//
//  RPIamgeCollectionViewCell.h
//  WorldOfTheFeed
//
//  Created by Ruslan on 7/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;

- (void)configureCellWithImageUrl:(NSString *)url;

@end
