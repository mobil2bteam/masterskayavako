//
//  RPOptions.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPOptions.h"

@implementation RPOptions

+ (RPOptions *) sharedOptions {
    static RPOptions* options = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        options = [[RPOptions alloc] init];
    });
    return options;
}

+ (void)initWithOptions: (RPOptions *)options{
    [RPOptions sharedOptions].about_text = options.about_text;
    [RPOptions sharedOptions].markers = options.markers;
    [RPOptions sharedOptions].books = options.books;
    [RPOptions sharedOptions].catalog = options.catalog;
}

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"about_text" toProperty:@"about_text"];
        [mapping hasMany:[RPMarker class] forKeyPath:@"markers" forProperty:@"markers"];
        [mapping hasMany:[RPCatalogue class] forKeyPath:@"catalog" forProperty:@"catalog"];
        [mapping hasMany:[RPBook class] forKeyPath:@"books" forProperty:@"books"];
    }];
}

@end

@implementation RPMarker

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"book_id",
                                          @"name",
                                          @"image",
                                          @"video_m",
                                          @"video_s"]];
    }];
}

@end

@implementation RPBook

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping hasMany:[RPBook class] forKeyPath:@"series" forProperty:@"series"];
        [mapping mapPropertiesFromArray:@[@"parrent_id",
                                          @"name",
                                          @"old",
                                          @"url_html",
                                          @"image",
                                          @"url_buy"]];
    }];
}

@end

//@implementation RPSerie
//
//+(EKObjectMapping *)objectMapping
//{
//    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
//        [mapping mapKeyPath:@"id" toProperty:@"ID"];
//        [mapping mapPropertiesFromArray:@[@"name",
//                                          @"url_html",
//                                          @"image",
//                                          @"url_buy"]];
//    }];
//}
//
//@end


@implementation RPCatalogue

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping mapPropertiesFromArray:@[@"name"]];
    }];
}

@end

