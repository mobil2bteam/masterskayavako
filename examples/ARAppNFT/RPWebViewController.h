//
//  RPWebViewController.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPWebViewController : UIViewController

@property (strong, nonatomic) NSString *url;

@end
