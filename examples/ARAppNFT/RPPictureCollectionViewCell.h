//
//  RPPictureCollectionViewCell.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPPictureCollectionViewCellDelegate.h"

@class RPMarker;

@interface RPPictureCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;

@property (weak, nonatomic) IBOutlet UILabel *pictureNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (assign, nonatomic) BOOL isGradientLayerAdded;

//кнопка для загрузки/удаления видео (default - загрузка, selected - удаление)
@property (weak, nonatomic) IBOutlet UIButton *downloadButton;

@property (weak, nonatomic) id<RPPictureCollectionViewCellDelegate> delegate;

@property (strong, nonatomic) CAGradientLayer *gradient;

@end
