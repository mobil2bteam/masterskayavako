//
//  RPSerieViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSerieViewController.h"
#import "RPBookCollectionViewCell.h"
#import "RPBookListViewController.h"
#import "RPOptions.h"

@interface RPSerieViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *booksCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *oldLabel;

@property (weak, nonatomic) IBOutlet UILabel *serieNameLabel;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@property (weak, nonatomic) IBOutlet UIView *centerBlueView;

@end

@implementation RPSerieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        //self.centerBlueView.layer.cornerRadius = 100;
    } else {

    }
    UIImage *patternImage = [UIImage imageNamed:@"pattern.png"];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:patternImage];
    [self.booksCollectionView registerNib:[UINib nibWithNibName:@"RPBookCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPBookCollectionViewCell class])];
    self.oldLabel.text = [NSString stringWithFormat:@"%ld+", (long)self.selectedBook.old];
    for (RPCatalogue *catalogue in [RPOptions sharedOptions].catalog) {
        if (catalogue.ID == self.selectedBook.parrent_id) {
            self.serieNameLabel.text = catalogue.name;
            break;
        }
    }
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.selectedBook.series.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPBookCollectionViewCell *cell = (RPBookCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPBookCollectionViewCell class]) forIndexPath:indexPath];
    [cell configureCellWith:self.selectedBook.series[indexPath.row]];
    [cell setBookNameLabelColor:[UIColor whiteColor]];
    cell.oldLabel.text = [NSString stringWithFormat:@"%ld+", (long)self.selectedBook.old];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UITabBarController *tabBarVC = ((UINavigationController *)self.presentingViewController).viewControllers[0];
    RPBookListViewController *bookListVC = tabBarVC.viewControllers[0];
    __weak typeof(self) weakSelf = self;
    [weakSelf dismissViewControllerAnimated:YES completion:^{
        [bookListVC didSelectBook:weakSelf.selectedBook.series[indexPath.row]];
    }];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height, width;
    height = CGRectGetHeight(self.booksCollectionView.bounds);
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
        width = (CGRectGetWidth(self.booksCollectionView.bounds) - 60)/4.5;
    } else {
        width = (CGRectGetWidth(self.booksCollectionView.bounds) - 30)/2.5;
    }
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}
- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
