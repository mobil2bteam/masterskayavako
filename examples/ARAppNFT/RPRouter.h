//
//  RPRouter.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RPOnboardViewController;
@class RPBook;

@interface RPRouter : NSObject

+ (RPRouter *)sharedRouter;

+ (void)setRootController;

+ (void)presentOnboardingViewControllerFromController:(UIViewController *)fromController;

+ (void)presentSerieViewControllerFromController:(UIViewController *)fromController selectedBook:(RPBook *)selectedBook;

+ (void)pushPictureListViewControllerFromController:(UIViewController *)fromController selectedBook:(RPBook *)selectedBook;

@end
