//
//  RPSerieViewController.h
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPBook;

@interface RPSerieViewController : UIViewController

@property (strong, nonatomic) RPBook *selectedBook;

@end
