//
//  RPLoadViewController.m
//  ARToolKit5iOS
//
//  Created by Ruslan on 8/12/16.
//
//

#import "RPLoadViewController.h"
#import "RPRouter.h"
#import <Masonry.h>
#import "RPOptions.h"

@interface RPLoadViewController ()

@end

@implementation RPLoadViewController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    // добавляем лого загрузки
    UIImageView *loadingImageView = [[UIImageView alloc]init];
    [self.view addSubview:loadingImageView];
    loadingImageView.image = [UIImage imageNamed:@"launch"];
    [loadingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
    }];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadJSONData];
    [self start];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

//считываем данные из файла
- (void)loadJSONData{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"augment" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    RPOptions *options = [EKMapper objectFromExternalRepresentation:dic withMapping:[RPOptions objectMapping]];
    [RPOptions initWithOptions:options];
}

- (void)start{
    if ([RPUserDefaults valueForKey:RPWasFirstLoad]) {
        [RPRouter setRootController];
    } else {
        [RPUserDefaults setBool:@YES forKey:RPWasFirstLoad];
        [RPUserDefaults synchronize];
        [RPRouter presentOnboardingViewControllerFromController:self];
    }
}

@end
